/*
Name of the project : Python Problems
File name : display.js
Description :  /*
Name of the project : Python Problems
File name : display.js
Description :  Write a GetNth() function that takes a linked list and an
               integer index and returns the data value stored in the node
               at that index position.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [5,2,6,3,7,8,2,9,2],3
Output : 3
*/


class Node {
    constructor(element) {
        this.element = element;
        this.next = null;
    }
}


class linkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    //function to add elements to the linked list
    addElement(num) {
        if(this.head == null) {
            var node = new Node(num);
            this.head = node;
            this.size ++;
        }else {
            var prev ;
            var current = this.head;
            while(current) {
                prev = current;
                current = current.next;
            }
            var node = new Node(num);
            prev.next = node;
            this.size ++;
        }
    }
    //function to display the linked list
    show() {
      var curr = this.head;
      var str = "";
      while (curr) {
         str += curr.element + " ";
         curr = curr.next;
      }
      console.log(str);
    }
    reverseList() {
        var root = this.head;
        this.rvrs(root);
    }
    rvrs(root) {
        if(root.next) {
            this.swap(root,root.next);
            if(root.next)
                root = root.next;
            else
                return;
            if(root.next)
                root = root.next;
            else
                return;
            this.rvrs(root);
        }else {
           return;
        }
    }
    swap(prv,root) {
      var temp;
      temp = prv.element;
      prv.element =root.element;
      root.element = temp;
    }
}


function display(arr) {
    var ll = new linkedList();
    for (var i = 0; i < arr.length; i++) {
      ll.addElement(arr[i]);
    }
    ll.show();
    ll.reverseList();
    ll.show();
}


display([1,2,3,4,5,6,7,8]);
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,2,3,4,5,6,7,8],2
Output : [2,1,4,3,6,5,8,7]
*/


class Node {
    constructor(element) {
        this.element = element;
        this.next = null;
    }
}
class linkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    pushElement(num) {
        var temp;
        if(this.head == null) {
            var node = new Node(num);
            this.head = node;
        }else {
            temp = this.head;
            var node = new Node(num);
            this.head = node;
            node.next = temp;
        }
    }
    getNth(key) {
        var current = this.head;
        for (var i = 0; i < key; i++) {
            current = current.next;
        }
        return current.element;
    }
    show() {
      var curr = this.head;
      var str = "";
      while (curr) {
         str += curr.element + " ";
         curr = curr.next;
      }
      console.log(str);
    }
}


function display(arr) {
    var out;
    var ll = new linkedList();
    for (var i = 0; i < arr.length; i++) {
      ll.pushElement(arr[i]);
    }
    ll.show();
    console.log(ll.getNth(3));
}


display([5,2,6,3,7,8,2,9,2]);
