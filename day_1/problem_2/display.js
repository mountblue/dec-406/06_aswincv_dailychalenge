/*
Name of the project : Dailychallenge
File name : display.js
Description : swap two nodes in linkedList
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [5,2,6,3,7,8,2,9,2]
Output : [5,2,6,8,7,3,2,9,2]
*/


class Node {
    constructor(element) {
        this.element = element;
        this.next = null;
    }
}
var reversList = [];
class linkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    addElement(num) {
        if(this.head == null) {
            var node = new Node(num);
            this.head = node;
            this.size ++;
        }else {
            var prev ;
            var current = this.head;
            while(current) {
                prev = current;
                current = current.next;
            }
            var node = new Node(num);
            prev.next = node;
            this.size ++;
        }
    }
    show() {
      var curr = this.head;
      var str = "";
      while (curr) {
         str += curr.element + " ";
         curr = curr.next;
      }
      console.log(str);
    }
    swap(key1,key2) {
        var temp1,temp2,temp3,temp;
        var prv,prv1,prv2;
        var flag1=true,flag2=true;
        var curr = this.head ;
        while(curr&&(flag1||flag2)) {
            console.log("hey");
            if(curr.element == key1) {
                flag1 = false ;
                prv1 = prv;
                temp1 = curr;
            }
            if( curr.element==key2 ) {
                prv2 = prv;
                flag2 = false ;
                temp2 = curr ;
            }
                prv = curr;
                curr = curr.next;
        }
        if(!flag1&&!flag2) {
            temp = prv1.next;
            prv1.next = temp2;
            temp3 = temp1.next;
            temp1.next = temp2.next;
            temp2.next =temp3;
            prv2.next = temp1;

        } else {
           console.log("sorry");
        }
    }
}


function display(arr) {
    var ll = new linkedList();
    for (var i = 0; i < arr.length; i++) {
      ll.addElement(arr[i]);
    }
    ll.show();
    ll.swap(3,8);
    ll.show();
}
display([5,2,6,3,7,8,2,9,2]);
