/*
Name of the project : Dailychallenge
File name : display.js
Description : to solve an expression given as tree
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : + * - 5 4 100 20
Output : 100
*/


var tree = [];
function display() {
    console.log(solve(0));
}
var result=0;
plus = "+";
minus = "-";
product = "*";
function solve(curr) {
    if(!(2*curr+2 > tree.length) ) {
        if(plus.localeCompare(curr.element)) {
            result = (solve(2*curr+1)) + (solve(2*curr + 2));
        }else if (product.localeCompare(curr.element)) {
            result = (solve(2*curr+1)) * (solve(2*curr + 2));
        }else if(minus.localeCompare(curr.element)){
            result = (solve(2*curr+1)) - (solve(2*curr + 2));
        }
        console.log(result);
        return result;
    } else {
        console.log(tree[curr]);
        return tree[curr];
    }
}
tree.push('+');
tree.push('*');
tree.push('-');
tree.push(5);
tree.push(4);
tree.push(100);
tree.push(20);
display();
