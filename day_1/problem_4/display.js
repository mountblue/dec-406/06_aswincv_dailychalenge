/*
Name of the project : Dailychallenge
File name : display.js
Description : level order insertion in a tree
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
*/


var tree = [];
level = 0 ;
function insert(key) {
    if(tree.length == 0) {
        tree.push(key);
    }else {
        var len = tree.length ;
        for (var i = 0; i < tree.length; i++) {
           if(tree[i] == null) {
               tree[i] = key;
           }else {
               tree.push(key);
           }
        }
    }
}
