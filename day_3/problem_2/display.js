/*
Name of the project : Dailychallenge
File name : display.js
Description : find the next node in binary tree
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,2,3,4,5,null,6,null,null,7,8],2
Output : 3
*/


function findNext(tree,node) {
    var i;
    for ( i = 0; i < tree.length; i++) {
        if(tree[i] == node)
            break;
    }
    for(var j = i+1; j < tree.length; j++) {
        if(tree[j] != null) {
            console.log(tree[j]);
            break;
        }
    }
    if(j==tree.length)
        console.log("null");
}


findNext([1,2,3,4,5,null,6,null,null,7,8],2);
findNext([1,2,3,4,5,null,6,null,null,7,8],5);
findNext([1,2,3,4,5,null,6,null,null,7,8],7);
findNext([1,2,3,4,5,null,6,null,null,7,8],8);
