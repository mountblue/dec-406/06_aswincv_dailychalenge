/*
Name of the project : Dailychallenge
File name : display.js
Description : find lowest common ancestor between two nodes from the given
              binary tree.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Input : [1,2,3,4,5,6,7,8,9],8,5
Output : 2
*/
var path =[[],[]];
function cmmnAnces(tree,node1,node2) {
    find(0,0,node1,tree);
    find(0,1,node2,tree);
    console.log(path[0]);
    console.log(path[1]);
    var i = path[0].length-1;
    var j = path[1].length-1;
    for( ; i>=0 ; i--) {
        j = path[1].length-1;
        for( ; j>=0 ; j--) {
            if(path[0][i] == path[1][j]) {
                console.log(path[0][i]);
                return;
            }else {
                continue;
            }
        }
    }
}


function find(i,pos,key,tree) {
    if(tree[i] == key) {
        return true;
    }else if(2*i+1<tree.length || 2*i+2 <tree.length) {
        path[pos].push(tree[i]);
        if(2*i+1 < tree.length && tree[2*i+1] != null) {
           if(find(2*i+1,pos,key,tree))
               return true;
        }else if(2*i+2 < tree.length && tree[2*i+2] != null) {
            if(find(2*i+2,pos,key,tree))
               return true;
        }
    }else {
       path[pos].pop();
       return false;
    }
}


cmmnAnces([1,2,3,4,5,6,7,8,9],8,5);
